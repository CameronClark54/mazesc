#pragma once
#include <SFML/Graphics.hpp>
#include "Player.h"
#include "Wall.h"
#include <vector>

class Level
{
public:
	//constructor
	Level();

	//functions
	void Input();
	void Update(sf::Time frameTime);
	void DrawTo(sf::RenderTarget& target);
	void LoadLevel(int levelNumber);

private:
	Player playerInstance;
	std::vector<Wall> wallInstances;
};

