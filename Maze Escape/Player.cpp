#include "Player.h"
#include "AssetManager.h"
#include <cmath>

Player::Player(sf::Vector2f startingPos):
	AnimatingObject(AssetManager::RequestTexture("Assets/Graphics/PlayerAnimation.png"),100,100,5.0f)
, velocity(0.0f, 0.0f)
, speed(300.0f)
, previousPosition(startingPos)
{
	sprite.setPosition(startingPos);

	//animates player movement
	AddClip("walkDown", 0, 3);
	AddClip("walkRight", 4, 7);
	AddClip("walkUp", 8, 11);
	AddClip("walkLeft", 12, 15);

	

}

void Player::Input()
{
	// Player keybind input
	velocity.x = 0.0f;
	velocity.y = 0.0f;
	bool hasInput = false;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		// Move player up
		velocity.y = -speed;
		//plays walking up animation
		if (hasInput == false)
		{
			PlayClip("walkUp");
		}
		hasInput = true;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		// Move player left
		velocity.x = -speed;
		//plays walking left animation
		if (hasInput == false) 
		{
			PlayClip("walkLeft");
		}
		hasInput = true;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		// Move player down
		velocity.y = speed;
		//plays walking down animation
		if (hasInput == false)
		{
			PlayClip("walkDown");
		}
		hasInput = true;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		// Move player right
		velocity.x = speed;
		//plays walking right animation
		if (hasInput == false)
		{
			PlayClip("walkRight");
		}
		hasInput = true;
	}

	if (hasInput == false) 
	{
		Stop();
	}
}

void Player::Update(sf::Time frameTime)
{
	//sets previous position
	previousPosition = sprite.getPosition();

	//calculate new position
	sf::Vector2f newPosition = sprite.getPosition() + velocity * frameTime.asSeconds();
	
	//move player to new position
	sprite.setPosition(newPosition);

	//allows for the player animation to update
	AnimatingObject::Update(frameTime);
}

void Player::HandleSolidCollision(sf::FloatRect otherHitbox)
{
	//check for collision
	bool isColliding = GetHitbox().intersects(otherHitbox);

	//if there is collision
	if (isColliding) 
	{
		// Calculate the collision depth (overlap)
		sf::Vector2f depth = CalculateCollisionDepth(otherHitbox);

		sf::Vector2f newPosition = sprite.getPosition();

		// Determine which is smaller - the x or y overlap
		if (std::abs(depth.x) < std::abs(depth.y)) 
		{
			// Calculate a new x coordinate such that the objects don't overlap
			newPosition.x -= depth.x;
		}
		else
		{
			// Calculate a new y coordinate such that the objects don't overlap
			newPosition.y -= depth.y;
		}
		// Move the sprite by the depth in whatever direction was smaller
		sprite.setPosition(newPosition);
	}

}
