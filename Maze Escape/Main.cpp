#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <string>
#include <vector>
#include <cstdlib>
#include <time.h>
#include "AssetManager.h"
#include "Player.h"
#include "SpriteObject.h"
#include "AnimatingObject.h"
#include "Wall.h"
#include"Level.h"

int main()
{
	// declare our SFML window, called gameWindow
	sf::RenderWindow gameWindow;
	// set up the SFML window, passing the dimmensions and the window name
	gameWindow.create(sf::VideoMode::getDesktopMode(), "Maze Escape", sf::Style::Titlebar | sf::Style::Close);

    
    //
    //GAME SETUP
    //


    
    //game clock
    //Create a clock to track time passed between frames
    sf::Clock gameClock;
    
    //seed random number generator
    srand(time(NULL));

	// Player setup
	//declare player object
	Player playerObject(sf::Vector2f(100.0f,100.0f));
	//sf::Sprite playerSprite;

	Wall wallObject(sf::Vector2f(500.0f, 500.0f));

	Level levelScreen();

	levelScreen.LoadLevel(1);

	// Player setup
	//declare player object
	/*AnimatingObject animatingObject(AssetManager::RequestTexture("Assets/Graphics/PlayerAnimation.png"),
		100,
		100,
		5.0f);*/
	//sf::Sprite animatingSprite;

    // Game Music
    sf::Music gameMusic;
    // load our audio using a file path
    gameMusic.openFromFile("Assets/Audio/music.ogg");//alter path
    // start the music
   //gameMusic.play();

    // Game Font
    sf::Font gameFont;
    // load our font using a file path
    gameFont.loadFromFile("Assets/Font/mainFont.ttf");//alter path
    
    //Set game over variable
    bool gameOver = false;



    //Game Loop
    
	while (gameWindow.isOpen())
	{
        //
        //INPUT SECTION
        //
        
		sf::Event gameEvent;
		while (gameWindow.pollEvent(gameEvent))
		{
			if (gameEvent.type == sf::Event::Closed)
			{
				gameWindow.close();
			}


		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
		{
			gameWindow.close();
		}

		/*if (gameOver) //|| winState) 
		{

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
			{
				gameWindow.close();
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
			{
				gameOver = false;
				winState = false;
				playerObject.Reset();
				score = 0;
				gameMusic.play();
				enemies.clear();
				playerBullets.clear();
				enemyBullets.clear();
				spawnIndex = 0;
			}
		}*/
        //player keybind input
        //playerObject.Input();
		levelObject.Input();
        
        //
        //UPDATE SECTION
        //
        
        //get time passed since last frame and reset game clock
        sf::Time frameTime = gameClock.restart();
        
        //update player
        //playerObject.Update(frameTime);
		levelObject.Update(frameTime);

		//collisions
		/*bool hit = false;
		if (playerObject.GetHitbox().intersects(wallObject.GetHitbox()))
		{
			hit = true;
		}*/

		playerObject.HandleSolidCollision(wallObject.GetHitbox());

        //
        //DRAW SECTION
        //
        
        //clear section to single colour
        gameWindow.clear(sf::Color(15,15,15));

		//draws player
		//playerObject.DrawTo(gameWindow);

		//draws wall
		wallObject.DrawTo(gameWindow);

		//draw level
		levelObject.DrawTo(gameWindow);

		//gameWindow.draw(playerSprite);

		//gameWindow.draw(animatingSprite);


		//animatingObject.DrawTo(gameWindow);
        
        //display window contents
        gameWindow.display();
	}

	return 0;
}