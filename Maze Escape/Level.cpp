#include "Level.h"
#include <iostream>
#include <fstream>


Level::Level()
:playerInstance(sf::Vector2f(0, 0))
, wallInstances()
{
	wallInstances.push_back(Wall(sf::Vector2f(200, 200)));
	wallInstances.push_back(Wall(sf::Vector2f(300, 300)));
	wallInstances.push_back(Wall(sf::Vector2f(400, 400)));
}

void Level::Input()
{
	playerInstance.Input();
}

void Level::Update(sf::Time frameTime)
{
	playerInstance.Update(frameTime);

	//checks for collisions
	for (int i = 0; i < wallInstances.size(); ++i) 
	{
		playerInstance.HandleSolidCollision(wallInstances[i].GetHitbox());
	}
}

void Level::DrawTo(sf::RenderTarget & target)
{
	playerInstance.DrawTo(target);

	for (int i = 0; i < wallInstances.size(); ++i)
	{
		wallInstances[i].DrawTo(target);
	}
}

void Level::LoadLevel(int levelNumber)
{
	// work out the file path based on the level number
	std::string filePath = "Assets/Levels/Level" + std::to_string(levelNumber) + ".txt";

	//open the level file
	std::ifstream inFile;
	inFile.open(filePath);

	if (!inFile)
	{
		std::cerr << "Unable to open file " + filePath;
		exit(1); // Call system to stop program with error
	}

	//clear level
	wallInstances.clear();

	//set starting x and y coordinates used to position objects
	float x = 0.0f;
	float y = 0.0f;
	//define the spacing we will use for our grid
	float xSpacing = 100.0f;
	float ySpacing = 100.0f;
	
	//read each character
	char ch;
	while (inFile >> std::noskipws >> ch) 
	{
		//perform actions based on what character has been read in
		if (ch == ' ')
		{
			//new column, increase x
			x += xSpacing;
		}

		else if (ch == '\n')
		{
			//new row, increase y and set x back to 0
			y += ySpacing;
			x = 0;
		}

		else if (ch == 'P')
		{
			//spawns player
			playerInstance.setPosition(sf::Vector2f(x, y));

		}

		else if (ch == 'W')
		{
			//creates walls
			wallInstances.push_back(Wall(sf::Vector2f(x, y)));
		}

		else if (ch == '-')
		{
			//nothingness
		}

		else
		{
			// Anything else is unrecognised
			std::cerr << "Unrecognised character in level file: " << ch;
		}
		
	}

	//close the file
	inFile.close();

}
