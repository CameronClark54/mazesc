#include "AnimatingObject.h"

AnimatingObject::AnimatingObject(sf::Texture & newTexture, int newFrameWidth, int newFrameHeight, float newFPS):
SpriteObject(newTexture)
, frameWidth(newFrameWidth)
, frameHeight(newFrameHeight)
, framesPerSecond(newFPS)
, currentFrame(0)
, timeInFrame(sf::seconds(0.0f))
, clips()
, currentClip("")
, playing(false)
{
}

void AnimatingObject::Update(sf::Time frameTime)
{
	//if its time for new frame
	timeInFrame += frameTime;
	sf::Time timePerFrame = sf::seconds(1.0f / framesPerSecond);
	if (timeInFrame >= timePerFrame) 
	{
		//update current frame
		++currentFrame;
		timeInFrame = sf::seconds(0);

		// If we get to end of clip
		Clip& thisClip = clips[currentClip];
		if (currentFrame > thisClip.endFrame)
		{
			// back to start of clip
			currentFrame = thisClip.startFrame;
		}
		
		//update sprite to use the correct frame of texture
		UpdateSpriteTextureRect();
	}
}

void AnimatingObject::AddClip(std::string name, int startFrame, int endFrame)
{
	//create new animation
	Clip& newClip = clips[name];

	//setup settings for this animation
	newClip.startFrame = startFrame;
	newClip.endFrame = endFrame;
}

void AnimatingObject::PlayClip(std::string name)
{
	//checks if current clip is already playing
	if (currentClip == name) 
	{
		//bail out
		return;
	}

	//find the clipss information in the map
	auto pairFound = clips.find(name);

	//if the clip exists
	if (pairFound != clips.end())
	{
		//set up animation based on clip
		currentClip = name;
		currentFrame = pairFound->second.startFrame;
		timeInFrame = sf::seconds(0.0f);
		playing = true;
		//update sprite rect
		UpdateSpriteTextureRect();
	}
}

void AnimatingObject::Pause()
{
	playing = false;
}

void AnimatingObject::Stop()
{
	playing = false;

	//reset to first frame of animation
	Clip& thisClip = clips[currentClip];
	currentFrame = thisClip.startFrame;
	UpdateSpriteTextureRect();
}

void AnimatingObject::Resume()
{
	if (!currentClip.empty()) 
	{
		playing = true;
	}
}

void AnimatingObject::UpdateSpriteTextureRect()
{

	//update sprite to use correct frame of the texture
	int numFramesX = sprite.getTexture()->getSize().x / frameWidth;
	int xFrameIndex = currentFrame % numFramesX;
	int yFrameIndex = currentFrame / numFramesX;
	sf::IntRect textureRect;
	textureRect.left = xFrameIndex * frameWidth;
	textureRect.top = yFrameIndex * frameHeight;
	textureRect.width = frameWidth;
	textureRect.height = frameHeight;
	sprite.setTextureRect(textureRect);
}
