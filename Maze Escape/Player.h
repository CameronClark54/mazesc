#pragma once
#include "AnimatingObject.h"
class Player :public AnimatingObject
{
public:
	// Constructors / Destructors
	Player(sf::Vector2f startingPos);

	// Functions
	void Input();
	void Update(sf::Time frameTime);
	void HandleSolidCollision(sf::FloatRect otherHitbox);

private:
	// Data
	sf::Vector2f velocity;
	float speed;
	sf::Vector2f previousPosition;
};

